module gitlab.com/tanna.dev/go-cobra-version-example

go 1.19

require (
	github.com/carlmjohnson/versioninfo v0.22.4
	github.com/spf13/cobra v1.6.1
)

require (
	github.com/inconshreveable/mousetrap v1.0.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
)
